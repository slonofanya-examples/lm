'use strict'
const bunyan = require('bunyan')
const bunyanFormat = require('bunyan-format')
const pkg = require(process.cwd() + '/package.json')
const _ = require('lodash')

function reqSerializer (req) {
  const headers = _.pick(req.headers, ['host', 'user-agent', 'x-real-ip'])

  let res = {
    method: req.method,
    url: req.url,
    headers: headers
  }

  if (!_.isEmpty(req.query)) { res.query = req.query }
  if (!_.isEmpty(req.params)) { res.params = req.params }
  if (!_.isEmpty(req.body)) { res.body = req.body }

  return res
}

function resSerializer (res) {
  const headers = _.pick(res._headers, ['content-length', 'content-type'])

  return {
    statusCode: res.statusCode,
    headers: headers,
    body: res.body
  }
}

function errSerializer (err) {
  if (!_.isObject(err)) return err

  let resErr = err.name || 'Error'
  resErr = resErr + ': '
  if (_.has(err, 'message') && !_.isEmpty(err.message)) {
    resErr = resErr + err.message
  }
  if (_.has(err, 'body.errors.base') && _.isArray(err.body.errors.base)) {
    resErr = resErr + err.body.errors.base.join(', ')
  }

  return resErr
}

const defaultOpts = {
  name: pkg.name,
  level: 'error',
  serializers: {
    req: reqSerializer,
    res: resSerializer
  }
}

function applyFormat (opts) {
  if (_.include(['development', 'test', 'stage'], process.env.NODE_ENV)) {
    const formatOut = bunyanFormat({outputMode: 'short'})
    const logLevel = process.env.LOG_LEVEL

    opts = _.extend(opts, {
      streams: [
        { stream : formatOut },
        {
          type: 'rotating-file',
          path: `${process.cwd()}/logs/${opts.name}.log`,
          period: '1d',   // daily rotation
          count: 3        // keep 3 back copies
        }
      ],
      level: logLevel,
      serializers: _.extend(defaultOpts.serializers, {
        err: errSerializer
      })
    })
  }
}

function createLogger (name) {
  let opts = _.cloneDeep(defaultOpts)
  opts.name = name
  applyFormat(opts)
  return bunyan.createLogger(opts)
}

let loggers = {}

module.exports = {
  get : (name) => {
    if (!name) {name = pkg.name}
    if (loggers[name]) return loggers[name]

    const newLogger = createLogger(name)
    loggers[name] = newLogger
    return newLogger
  }
}
