'use strict'
const $require = require('../require')
const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const requireCfg = require('../requireCfg')

const routes = $require('routes-metrics')
const expressErrHandlers = $require('lib/responders/err-handlers')
const expressSendResponse = $require('lib/responders/send-response')
const requestLogger = $require('lib/middleware/request-logger')()

const serverCfg = requireCfg('metrics')

const port = serverCfg.port || 3000

function CORS (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
}

module.exports = () => {
  return new Promise((resolve, reject) => {
    const app = express()

    app.set('port', port)
    app.set('x-powered-by', false)
    app.set('query parser', 'extended')

    app.use(methodOverride())
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))

    app.use(CORS)
    app.use(routes.router)
    app.use(requestLogger)

    app.use(expressSendResponse.router)
    expressErrHandlers.use(app)

    resolve(app)
  })
}
