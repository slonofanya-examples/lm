'use strict'
const redisCfg = require('konphyg')(`${process.cwd()}/config`)('redis')
const redis = require('redis')
const log = require('../logger').get('[redis]')

let client = null

module.exports = () => {
  return new Promise ((resolve, reject) => {
    const host = `http://${redisCfg.host}:${redisCfg.port}`
    log.info(`Connecting to Redis on ${host}`)
    client = redis.createClient(redisCfg.port, redisCfg.host)

    client.on('error', (err) => {
      log.error(err)
      reject(err)
    })

    client.once('ready', () => {
      log.info(`Connected to Redis on ${host}`)
      log.info(`Selecting [1] db ...`)
      client.select(1, () => {
        log.info(`Selected [1] db!`)
        resolve(client)
      })
    })
  })
}
