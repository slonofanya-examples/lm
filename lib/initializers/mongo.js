'use strict'
const mongoCfg = require('konphyg')(`${process.cwd()}/config`)('mongo')

const mongoose = require('mongoose')
const bluebird = require('bluebird')
const requireTree = require('require-tree')
const log = require('../logger').get('[mongo]')

requireTree(`${process.cwd()}/models`)

module.exports = () => {
  const deferred = bluebird.defer()

  log.info(`Connecting to MongoDB on ${mongoCfg.uri}`)
  mongoose.connect(mongoCfg.uri)

  const db = mongoose.connection
  db.on('error', (err) => {
    // TODO: error reporters check
    // errorReporter(err)
    log.error(err)
    deferred.reject(err)
  })

  db.once('open', () => {
    log.info(`Connected to MongoDB on ${mongoCfg.uri}`)
    deferred.resolve(db)
  })

  return deferred.promise
}
