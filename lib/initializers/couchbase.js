'use strict'
const couchCfg = require('konphyg')(`${process.cwd()}/config`)('couchbase')
const couchbase = require('../adapters/couchbase')
const log = require('../logger').get('[couchbase]')

module.exports = () => {
  log.info(`Connecting to Couchbase on ${couchCfg.uri}`)

  return couchbase.connect(couchCfg.uri)
    .then((bucket) => {
      log.info(`Connected to Couchbase on ${couchCfg.uri}`)
      return bucket
    })
    .catch(log.error)
}