'use strict'
const $require = require('../require')
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const favicon = require('serve-favicon')
const path = require('path')
const methodOverride = require('method-override')
const compress = require('compression')
const layouts = require('handlebars-layouts')
const hbs = require('hbs')
const config = require('konphyg')(process.cwd() + '/config')
const hbsUtils = require('hbs-utils')(hbs)

const routes = $require('routes')
//import expressReduxHandler from '../responders/redux-handler'
const expressErrHandlers = $require('lib/responders/err-handlers')
const expressSendResponse = $require('lib/responders/send-response')
const requestLogger = $require('lib/middleware/request-logger')()

hbs.handlebars.registerHelper(layouts(hbs.handlebars))
hbsUtils.registerPartials(process.cwd() + '/views/partials')

const serverCfg = config('server')
const secretsCfg = config('secrets')

const port = serverCfg.port || 3000
const uploadsDir = serverCfg.uploadsDir

function CORS (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
}

module.exports = () => {
  return new Promise((resolve, reject) => {
    const app = express()

    app.set('port', port)
    app.set('uploadsDir', uploadsDir)
    app.set('x-powered-by', false)
    app.set('query parser', 'extended')
    app.use(compress())

    app.set('views', path.join(process.cwd(), 'views'))
    app.set('view engine', 'hbs')

    app.use(favicon(path.join(process.cwd(), 'public/icons/favicon.ico')))

    app.set('uploadsDir', path.resolve(path.join(process.cwd(), uploadsDir)))

    app.use(methodOverride())
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(cookieParser(secretsCfg.cookie))

    app.use('/public', express.static(path.join(process.cwd(), 'public')))
    app.use('/public', express.static(path.join(process.cwd(), 'build')))
    app.use('/public/fonts', express.static(path.join(process.cwd(), 'node_modules/bootstrap-styl/fonts')))

    app.use(CORS)
    app.use(routes.router)
    app.use(requestLogger)

    app.use(expressSendResponse.router)
    expressErrHandlers.use(app)

    resolve(app)
  })
}
