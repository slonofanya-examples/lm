'use strict'
var fs = require('fs')

const secrets = {
  'cookie': 'my-new-project',
  'jwt': {
    'key': 'secret-key',
    'options': {
      'expiresIn': 3*24*60*60  // 3d
    }
  }
}

const data = JSON.stringify(secrets, null, 2)
fs.writeFileSync(`${process.cwd()}/config/secrets.json`, data)

console.log('secrets.json created')
process.exit(0)
