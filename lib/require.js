'use strict'

const path = require('path')
const fs = require('fs')
const find = require('find')
const _ = require('lodash')

const basePath = process.cwd()
const requireCfg = require('konphyg')(`${basePath}/config`)

global.$require = modulePath => {
  return require(path.join(basePath, modulePath))
}

global.$requireConfig = sectionName => {
  return requireCfg(sectionName)
}

global.$requireConfigFromDir = dir => {
  const configPath = path.join(basePath, dir)
  const config = { }

  const testBigDataFiles = find.fileSync(configPath)

  _.forEach(testBigDataFiles, filePath => {
    const testDataKey = _.camelCase(path.basename(filePath, 'json'))
    const testDataValue = fs.readFileSync(filePath)

    config[testDataKey] = JSON.parse(testDataValue.toString())
  })

  return config
}
