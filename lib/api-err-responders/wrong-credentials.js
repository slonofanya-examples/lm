'use strict'
module.exports = (reason) => {
  reason = reason || 'wrong credentials'
  var err = new Error(reason)
  err.name = 'Unauthorized'
  err.code = 401
  return err
}
