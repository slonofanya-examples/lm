'use strict'
const _ = require('lodash')

module.exports = (id, resourceName) => {
  id = id || ''
  resourceName = resourceName || ''
  resourceName = _.isEmpty(resourceName) ? '' : resourceName + ':'

  const err = new Error('resource [' + resourceName + id + '] not found')
  err.code = 404
  err.name = 'Not found'

  return err
}
