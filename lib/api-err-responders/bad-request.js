'use strict'
module.exports = (msg) => {
  msg = msg ? msg : 'bad request'

  const err = new Error(msg)
  err.code = 400
  err.name = 'Bad request'

  return err
}
