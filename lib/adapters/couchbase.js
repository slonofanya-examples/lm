'use strict'
const $require = require('../require')
const _ = require('lodash')
const bluebird = require('bluebird')
const log = $require('lib/logger').get('[couchbase]')

let couchbase = require('couchbase')

if (process.env.COUCHBASE_MODE !== 'real' && process.env.NODE_ENV === 'test') {
  couchbase = couchbase.Mock
}

let clusters = []
let currCluster = null
let currBucket = null

function connect (uri, bucketName) {
  bucketName = bucketName ? bucketName : 'default'

  const deferred = bluebird.defer()
  currCluster = _.findWhere(clusters, { uri: uri })

  if (!currCluster) {
    let cluster = new couchbase.Cluster(uri)

    currCluster = {
      uri: uri,
      cluster: cluster,
      buckets: {}
    }

    clusters.push(currCluster)
  }

  if (!_.has(currCluster.buckets, bucketName)) {
    currBucket = currCluster.cluster.openBucket(bucketName)
    currCluster.buckets[bucketName] = currBucket

    currBucket.on('error', (err) => {
      log.error(err)
      deferred.reject(errInternalErr(err))
    })

    currBucket.once('connect', () => {
      deferred.resolve(currBucket)
    })

  } else {
    deferred.resolve(currCluster.buckets[bucketName])
  }

  return deferred.promise
}

function selectBucket (bucketName, clusterUri) {
  if (currBucket && !bucketName) { return currBucket }

  if (clusters.length === 1 && bucketName) {
    return clusters[0].buckets[bucketName]
  }

  if (bucketName && clusterUri) {
    const cluster = _.findWhere({ uri: clusterUri })
    currBucket = cluster.buckets[bucketName]
    return currBucket
  }

  return false
}

module.exports = {
  connect,
  selectBucket,
}