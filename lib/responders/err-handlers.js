'use strict'
const log = require('../logger').get()

function flattenValidationError (err) {
  var errors = {}
  for (var key in err.errors) {
    if (err.errors[key].type === 'required') {
      errors[err.errors[key].path] = [`${err.errors[key].path} is required`]
    } else {
      if (err.errors[key].name === 'ValidationError') {
        var nestedErrors = flattenValidationError(err.errors[key])
        for (var nestedKey in nestedErrors) {
          errors[`${key}.${nestedKey}`] = nestedErrors[nestedKey]
        }
      } else {
        errors[err.errors[key].path] = [err.errors[key].message]
      }
    }
  }
  return errors
}

function default404Handler (req, res, next) {
  res.status(404).json({
    error: 'Not found: ' + req.method + ' ' + req.originalUrl
  })
}

function mongoHandler (err, req, res, next) {
  if (err.name === 'ValidationError') {
    var validationErrors = flattenValidationError(err)
    return res.status(422).json({ errors: validationErrors })
  }

  if (err.name === 'MongoError' && err.code === 11000) {
    var parts = err.message.split('$')
    var fieldName = parts.pop().split('_').shift()
    var duplicateErrors = {}
    duplicateErrors[fieldName] = [fieldName + ' already in use']
    return res.status(422).json({ errors: duplicateErrors })
  }

  next(err)
}

function couchHandler (err, req, res, next) {
  if (err.name === 'CouchbaseError' || err.name === 'CbError') {
    delete err.name
    return res.status(422).json({ errors: err })
  }

  next(err)
}

function defaultHandler (err, req, res, next) {
  err.code = err.code || 500
  if (err.code === 500) {
    log.error({ err: err })
    log.error(err.stack)
  } else {
    log.warn({ err: err })
  }
  return res.status(err.code).json({ error: err.message })
}

function useApp (app) {
  app.use(default404Handler)
  // mongo validation error transformation
  // note that this should be before default error handler
  app.use(mongoHandler)
  app.use(couchHandler)
  app.use(defaultHandler)
}

module.exports = { use: useApp }