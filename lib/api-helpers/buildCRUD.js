'use strict'
const $require = require('../require')
const _ = require('lodash')
const express = require('express')
const applyParamsCheck = $require('lib/api-helpers/applyParamsCheck')
const errResNotFound = require('../api-err-responders/resource-not-found')

let actionsMapping = {
  'create': 'POST',
  'list': 'GET',
  'retrieve': 'GET /:objectId',
  'retrieve-set': 'GET /$idListRegExp',
  'update': 'PUT /:objectId',
  'remove': 'DELETE /:objectId',
  'patch': 'PATCH /:objectId'
}

/**
 *
 * @param Model
 * @param opts.actions - { create:false } will cause to disable POST action mapping
 * @param opts.populate - ['propName']
 * @returns {*}
 */
module.exports = (Model, options) => {
  var router = express.Router()
  options = options || {}

  var actions = {
    'GET': (req, res, next) => {
      if (!req.pattern) req.pattern = {}
      req.pattern.deleted = { $ne: true }
      var limit = parseInt(req.query.limit, 10)

      if (limit && !isNaN(limit)) {
        var paginationQuery = {}
        if (req.query.cursor) {
          paginationQuery['before'] = req.query.cursor.toString()
        }
        if (options.sort) {
          paginationQuery['sort'] = options.sort
        }
        var paginatedQuery = Model.paginate(paginationQuery)
          .where(req.pattern)
          .limit(limit)
        if (options.populate) {
          paginatedQuery.populate(options.populate)
        }

        paginatedQuery.execPagination((err, result) => {
          if (err) return next(err)
          res.body = {
            cursor: result.before,
            limit: limit,
            items: result.results,
            total: result.thisPage
          }
          next()
        })
      } else {
        var allQuery = Model.find(req.pattern)
        if (options.populate) {
          allQuery.populate(options.populate)
        }
        if (options.sort) {
          allQuery.sort(options.sort)
        }
        allQuery.exec((err, models) => {
          if (err) return next(err)
          res.body = models
          next()
        })
      }
    },
    'GET /:objectId': (req, res, next) => {
      req.pattern = req.pattern || {}
      req.pattern._id = req.params.objectId
      req.pattern.deleted = { $ne: true }
      Model.findOne(req.pattern, (err, model) => {
        if (err) return next(err)
        res.body = model
        if (!options.populate || !model || model.deleted) {
          return next()
        }
        model.populate(options.populate, next)
      })
    },
    'GET /$idListRegExp': (req, res, next) => {
      req.pattern = req.pattern || {}
      req.pattern._id = { $in: req.params[0].split(';') }
      req.pattern.deleted = { $ne: true }

      var query = Model.find(req.pattern)
      if (options.populate) {
        query.populate(options.populate)
      }

      query.exec((err, model) => {
        if (err) return next(err)
        res.body = model
        next()
      })
    },
    'POST': (req, res, next) => {
      req.body.creator = req.session.userId
      transformRequestBody(req.body)
      Model.create(req.body, (err, model) => {
        if (err) return next(err)
        res.body = model
        if (!options.populate) {
          return next()
        }
        model.populate(options.populate, next)
      })
    },
    'PUT /:objectId': (req, res, next) => {
      req.pattern = req.pattern || {}
      req.pattern._id = req.params.objectId
      transformRequestBody(req.body)
      Model.findOne(req.pattern, (err, model) => {
        if (err) return next(err)
        if (!model || model.deleted) return next(errResNotFound(req.params.objectId))
        res.body = model
        res.body.set(req.body)
        res.body.save((err) => {
          if (err) return next(err)
          if (!options.populate) {
            return next()
          }
          model.populate(options.populate, next)
        })
      })
    },
    'PATCH /:objectId': (req, res, next) => {
      req.pattern = req.pattern || {}
      req.pattern._id = req.params.objectId
      transformRequestBody(req.body)
      Model.findOne(req.pattern, (err, model) => {
        if (err) return next(err)
        if (!model || model.deleted) return next(errResNotFound(req.params.objectId))
        res.body = model
        res.body.set(req.body)
        res.body.save((err) => {
          if (err) return next(err)
          if (!options.populate) {
            return next()
          }
          model.populate(options.populate, next)
        })
      })
    },
    'DELETE /:objectId': (req, res, next) => {
      Model.findById(req.params.objectId, (err, model) => {
        if (err) return next(err)
        if (!model) return next(errResNotFound(req.params.objectId))
        model.deleted = true
        model.save((err) => {
          if (err) return next(err)
          res.body = model
          next()
        })
      })
    }
  }

  if (options.populate) {
    options.populate = options.populate.map((value) => {
      if (_.isString(value)) {
        return {
          path: value,
          match: {deleted: {$ne: true}}
        }
      }
      return value
    })
  }

  function transformRequestBody (body) {
    for (var property in Model.schema.tree) {
      let pDefinition = Model.schema.tree[property][0] || Model.schema.tree[property]
      if (body[property] && pDefinition.ref) {
        if (!Array.isArray(body[property])) {
          body[property] = body[property].id || body[property]
        } else {
          body[property] = body[property].map((i) => {
            return i.id || i
          })
        }
      }
    }
  }

  applyParamsCheck(router)

  if (options.actions) {
    let preActions = {}
    let postActions = {}

    for (var name in options.actions) {
      const hasNoDash = !name.includes('-')

      // pass: 'any': [], skip: 'pre-any' | 'post-any'
      if (_.isArray(options.actions[name]) && hasNoDash) {
        actions[actionsMapping[name]] = options.actions[name]
        continue
      }

      // replace predefined action with new one; skip: 'pre-any' | 'post-any'
      if (_.isFunction(options.actions[name]) && hasNoDash) {
        actions[actionsMapping[name]] = options.actions[name]
        continue
      }

      // deletes 'name' action if opts.actions.name === false
      if (options.actions[name] === false && hasNoDash) {
        delete actions[actionsMapping[name]]
        continue
      }

      var prefix = name.split('-').shift()
      var endings = name.split('-').pop()
      var preName = null
      var postName = null

      // setting 'name' action to be pre-* actions
      if (prefix === 'pre' && endings === '*') {
        for (preName in actions)
          preActions[preName] = _.flatten([ preActions[preName] || [], options.actions[name] ])
        continue
      }

      // setting 'name' action to be post-* actions
      if (prefix === 'post' && endings === '*') {
        for (postName in actions)
          postActions[postName] = _.flatten([ postActions[postName] || [], options.actions[name] ])
        continue
      }

      // setting pre-'name' action
      if (prefix === 'pre') {
        preName = actionsMapping[endings]
        if (!preName) throw new Error('action not found ' + name)
        preActions[preName] = _.flatten([ preActions[preName] || [], options.actions[name] ])
        continue
      }

      // setting post-'name' action
      if (prefix === 'post') {
        postName = actionsMapping[endings]
        if (!postName) throw new Error('action not found ' + name)
        postActions[postName] = _.flatten([ postActions[postName] || [], options.actions[name] ])
        continue
      }
    }

    // merging all actions to one path
    for (var actionName in actions) {
      actions[actionName] = _.flatten([
        preActions[actionName] || [],
        actions[actionName],
        postActions[actionName] || []
      ])
    }
  }

  _.each(actions, (handlers, path) => {
    var pathChunks = path.split(' ')
    var method = pathChunks[0]
    var query = pathChunks[1] || '/'
    if (query === '/$idListRegExp') {
      query = /([0-9a-fA-F;]{25,256})/
    }

    router[method.toLowerCase()](query, handlers)
  })

  return router
}

module.exports.actionsMapping = actionsMapping
