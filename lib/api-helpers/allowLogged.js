'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const User = $require('models/user')
const errResNotFound = require('../api-err-responders/resource-not-found')
const errResNotAuthorized = require('../api-err-responders/not-authorized')

module.exports = (req, res, next) => {
  if (!req.session.userId) {
    return next(errResNotAuthorized())
  }

  if (!req.user) {
    User.findById(req.session.userId, (err, user) => {
      if (err) return next(err)
      if (!user) return next(errResNotFound(req.session.userId, 'User'))
      req.user = user
      next()
    })
  } else {
    next()
  }
}
