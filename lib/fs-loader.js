var realFs = require('fs')
var gracefulFs = require('graceful-fs')

gracefulFs.gracefulify(realFs)

module.exports = realFs