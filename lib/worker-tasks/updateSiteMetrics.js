'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const Metric = $require('models/metric')
const log = $require('lib/logger').get('Worker')
const _ = require('lodash')
const metricsConfig = require('konphyg')(process.cwd() + '/config')('metrics')
const Promise = require('bluebird')

let redisClient = null
const newCommentsBucketNumber = 2
const promoCommentsBucketNumber = 3

function selectRedisBucket (bucketNumber) {
  return new Promise(resolve => {
    redisClient.select(bucketNumber, () => resolve(redisClient.keysAsync('SITE_*')))
  })
}

function getRedisValues (resKeys) {
  let res = {
    keys: [],
    values: []
  }

  if (!resKeys.length) { return res }

  return redisClient.mgetAsync(resKeys).then(resValues => {
    res.keys = resKeys
    res.values = resValues
    return res
  })
}

function redisArraysToObject (res) {
  let sites = {}

  log.info(`Received [${res.values.length}] comments`)

  const shortNames = _.map(res.keys, (key) => { return key.split('_')[1] })

  shortNames.forEach((name, idx) => {
    sites[name] = parseInt(res.values[idx])
  })

  return sites
}

function saveInMongo (sites, metricType) {
  _.forEach(sites, (value, siteShortName) => {
    Metric.create({
      metricType: metricType,
      siteShortName,
      value
    })
      .then(() => {
        log.info(`Saved in mongo: ${siteShortName}`)
      })
    .catch(log.error)
  })
}

function getCommentsFromRedis (bucketNumber) {
  const metricType = bucketNumber == newCommentsBucketNumber ? 'newComments' : 'promoComments'

  log.info(`Query Redis for ${metricType}`)

  return selectRedisBucket(bucketNumber)
    .then(getRedisValues)
    .then(redisArraysToObject)
    .then(sites => saveInMongo(sites, metricsConfig.types[metricType]))
    .tap(() => {redisClient.flushdb()})
}


function updateComments (redis) {
  if (!redisClient) redisClient = redis

  return getCommentsFromRedis(newCommentsBucketNumber)
    .then(() => getCommentsFromRedis(promoCommentsBucketNumber))
    .catch(log.error)
}

function updateSiteMetrics (redisClient) {
  log.info('Start updating Site Metrics')

  return updateComments(redisClient)
    .then(() => {
      log.info('End updating Site Metrics')
    })
}

module.exports = updateSiteMetrics