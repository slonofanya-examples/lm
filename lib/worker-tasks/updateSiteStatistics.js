const $require = require(`${process.cwd()}/lib/require`)
const _ = require('lodash')
const log = $require('lib/logger').get('Worker')
const Site = $require('models/site')
const SiteStat = $require('models/siteStat')

/**
 * Query Redis for site codes and request count
 * @param {object} sites - { 'site-code' : {int}REQUEST_COUNT }
 */
function queryRedis (client) {
  log.info('Query Redis...')
  let resKeys
  let resValues

  return client.keysAsync('SITE_*')
    .tap((res) => { resKeys = res })
    .then(() => {
      if (resKeys.length > 0) {
        return client.mgetAsync(resKeys)
      } else {
        return []
      }
    })
    .tap((res) => { resValues = res })
    .then(() => { return client.timeAsync() })
    .then((time) => {
      log.info(`Received [${resValues.length}] key-values pairs`)

      const shortNames = _.map(resKeys, (key) => { return key.split('_')[1] })
      const values = _.map(resValues, (val) => { return parseInt(val) })

      let res = {}
      shortNames.forEach((name, idx) => {
        res[name] = { calls: values[idx] }
      })

      return {
        timestamp: parseInt(time[0]) * 1000,
        sitesStat: res
      }
    })
    .then((data) => {
      client.flushdb()
      return data
    })
}

/**
 * Query CouchBase for site details by site code
 * @param {object} data - { timestamp, sitesStat : { SHORT_NAME: { calls: {int}REQUEST_COUNT] } }
 */
function queryCouchbase (bucket) {
  return (data) => {
    log.info('Query Couchbase...')

    const shortNames = _.keys(data.sitesStat)

    const promises = _.map(shortNames, (shortName)=> {
      return new Promise((resolve, reject) => {
        bucket.get(`site_${shortName}`, (err, res) => {
          if (err) {
            return resolve({
              name: '',
              url: '',
              shortname: shortName
            })
          }

          const site = _.pick(res.value, ['name', 'url', 'shortname'])
          resolve(site)
        })
      })
    })

    return Promise.all(promises)
      .then((sites) => {
        let res = {}
        sites.forEach((site) => { res[site.shortname] = site })

        data.sites = res
        return data
      })
  }
}

/**
 * Update Mongo site statistics
 * @param {object} data - {
 *                          timestamp,
 *                          sites: { SHORT_NAME: {<sites-details>} },
 *                          sitesStat: { SHORT_NAME : { calls: {int}REQUEST_COUNT } }
 *                        }
 *  @param site-details : {name, url, shortname}
 */
function updateMongo (data) {
  let sitesUpdatePromises = []
  const shortNames = _.keys(data.sites)

  shortNames.forEach((shortName) => {
    if (!shortName || _.isEmpty(data.sites[shortName].url)) { return }

    sitesUpdatePromises.push(new Promise((resolve, reject) => {
      Site.findOneAndUpdate({ shortname: shortName }, data.sites[shortName], { upsert: true })
        .exec((err, res) => {
          if (err) {
            log.error(err)
            return resolve(-1)
          }

          data.sites[shortName].model = res
          resolve(true)
        })
    }))
  })

  log.info('Updating Mongo with data (sample):')
  let demo = _.cloneDeep(data)
  demo.sites = _.sample(demo.sites, 3)
  demo.sitesStat = _.sample(demo.sitesStat, 3)
  log.info(JSON.stringify(demo))

  log.info('Updating sites...')
  return Promise.all(sitesUpdatePromises)
    .then((res) => {
      log.info('Updating sites stats...')

      let statsUpdatePromises = []

      shortNames.forEach((shortName) => {
        statsUpdatePromises.push(new Promise((resolve, reject) => {
          if (!data.sites[shortName].model) {return resolve(-1)}

          const payload = {
            _creator: data.sites[shortName].model._id,
            timestamp: data.timestamp,
            calls: data.sitesStat[shortName].calls
          }

          SiteStat.create(payload, (err, stat) => {
            if (err) {
              log.error(err)
              return resolve(-1)
            }

            resolve(true)
          })
        }))
      })

      return Promise.all(statsUpdatePromises)
    })
    .then((res) => {
      log.info('Mongo updated successfully!')
    })
}

function updateSiteStatistic (redisClient, couchClient) {
  log.info('Start updating Site Statistic')

  return queryRedis(redisClient)
    .then(queryCouchbase(couchClient))
    .then(updateMongo)
    .then(() => {
      log.info('End updating Site Statistic')
    })
  .catch(console.log)
}

module.exports = updateSiteStatistic