const updateSiteMetrics = require('./updateSiteMetrics')
const updateSiteStatistics = require('./updateSiteStatistics')

module.exports = {
  updateSiteMetrics,
  updateSiteStatistics,
}