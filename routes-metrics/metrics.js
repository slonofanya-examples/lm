'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const createImpressionCtrl = $require('controllers/metrics/impressions/create')
const express = require('express')

let router = new express.Router()

router.post('/metrics/impressions', createImpressionCtrl)

module.exports = { router }