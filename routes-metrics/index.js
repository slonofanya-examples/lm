'use strict'
const _ = require('lodash')
const express = require('express')
const requireCfg = require('../lib/requireCfg')
let router = new express.Router()
let apiRouter = new express.Router()

const serverCfg = requireCfg('metrics')

const metrics = require('./metrics')

apiRouter.use(metrics.router)

router.use(serverCfg.api.mountPoint, apiRouter)

module.exports = { router }