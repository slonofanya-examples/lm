import React from 'react'
import { Route, Redirect, IndexRoute } from 'react-router'
import RouterContainer from './containers/RouterContainer'
import $ from 'jquery'

/* containers */
import App from './containers/App.js'

/* partials */
import SitesMetrics from './pages/SitesMetrics'
import Login from './pages/Login'

function onEnter () { $.notifyClose() }

export default (store) => {
  return (
    <Route path='/app' component={App}>
      <IndexRoute component={SitesMetrics} onEnter={RouterContainer.requireAuth(store)}/>
      <Route path='login' component={Login} onEnter={onEnter}/>
      <Route path='sites-metrics' component={SitesMetrics} onEnter={RouterContainer.requireAuth(store)}/>
      <Redirect from='/' to='/app/sites-metrics'/>
      <Redirect from='/app' to='/app/sites-metrics'/>
    </Route>
  )
}