import React from 'react'
import { connect } from 'react-redux'
import * as sitesMetricsActions from '../actions/SitesMetricsActions'
import { isFetched, isPeriodFetched } from '../reducers/reports/SitesMetricsReducer'
import { bindActionCreators } from 'redux'
import moment from 'moment'

import Table from '../components/MetricTable'
import DatePicker from '../components/inputs/DatePicker'
import TopNavbar from '../components/TopNavbar'
import Waiter from '../helpers/Waiter'

function select (state) {
  return {
    sitesMetrics: state.reports.sitesMetrics,
    sitesRatings: state.reports.sitesRatings,
  }
}

function actions (dispatch) {
  return {
    actions: {
      fetchMetrics: bindActionCreators(sitesMetricsActions['fetchMetrics'], dispatch),
      fetchRatings: bindActionCreators(sitesMetricsActions['fetchRatings'], dispatch),
    }
  }
}

@connect(select, actions)
class SitesMetrics extends React.Component {
  constructor (props) {
    super(props)

    if (!props.sitesMetrics.period) {
      props.sitesMetrics.period = {
        from: moment().startOf('month'),
        till: moment()
      }
    }
  }

  componentDidMount () {
    if (isFetched(this.props.sitesMetrics)) { return }

    return setTimeout(() => {
      this.updateMetrics(this.props.sitesMetrics.period)
    }, 0)
  }

  isFetched () {
    return isFetched(this.props.sitesMetrics)
  }

  isPeriodFetched () {
    return isPeriodFetched(this.props.sitesMetrics)
  }

  updateMetrics (dateRange) {
    return Promise.all([
      this.props.actions.fetchMetrics({
        from: dateRange.from,
        till: moment.unix(dateRange.till).add(1, 'days').unix()
      }),
      this.props.actions.fetchRatings({
        from: dateRange.from * 1000,
        till: moment.unix(dateRange.till).add(1, 'days').valueOf()
      }),
    ])
  }

  mergeSites () {
    let sites = this.props.sitesRatings.sites || []

    _.map(this.props.sitesMetrics.sites, siteMetrics => {
      let site = _.findWhere(sites, {shortname: siteMetrics.shortname})

      if (!site) {
        siteMetrics.totalCalls = 0
        sites.push(siteMetrics)
      } else {
        Object.assign(site, {
          impressions: siteMetrics.impressions,
          newComments: siteMetrics.newComments,
          promoComments: siteMetrics.promoComments,
        })
      }
    })

    return sites
  }

  render () {
    let noData = null
    const sites = this.mergeSites()

    if (this.isFetched()) {
      const { isFetching } =  this.props.sitesMetrics

      if (sites.length === 0 && !isFetching) {
        noData = (
          <div style={{ width:'100%', textAlign: 'center' }}>
            <h2>No data.</h2>
          </div>
        )
      }
    }

    const newPeriod = {
      from: moment(this.props.sitesMetrics.period.from),
      till: moment(this.props.sitesMetrics.period.till),
    }

    return (
      <div>
        <TopNavbar />
        <div className='container-fluid'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-3 col-sm-3'>
                {this.isPeriodFetched()
                  ? <DatePicker {...newPeriod} onChange={this.updateMetrics.bind(this)}/>
                  : <Waiter/>
                }
              </div>
            </div>
            <div className='row'>
              <div className='col-md-12 col-sm-12'>
                { this.isFetched()
                  ? (noData ? noData : <Table sites={sites} />)
                  : <Waiter/>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default SitesMetrics