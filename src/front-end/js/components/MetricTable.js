import React from 'react'
import uuid from 'node-uuid'
import createFragment from 'react-addons-create-fragment'
import { Icon } from '../helpers/FontAwesome.js'
import { Table } from 'react-bootstrap'

class MetricTable extends React.Component {
  defaultSortWay = 'desc'

  constructor (props) {
    super(props)

    this.state = {
      sorting: this.defaultSortWay,
      sortField: 'url',
    }
  }

  handleSort (e) {
    e.preventDefault()
    const $el = $(e.target).parent()

    if ($el.hasClass('disabled')) { return }

    $el.closest('tr').find('th a').not($el).data('sort-asc', null)
    $el.data('sort-asc', !$el.data('sort-asc'))

    this.setState({
      sortField: $el.data('sort-field'),
      sorting: $el.data('sort-asc') ? 'asc' : 'desc',
    })
  }

  renderSortIcon (fieldName, iconType) {
    const sortingWay = this.state.sortField == fieldName ? this.state.sorting : this.defaultSortWay

    return (
      <a onClick={this.handleSort.bind(this)}
         data-sort-field={fieldName}
         href=""
      >
        <Icon name={`sort-${iconType}-${sortingWay}`} fw/>
      </a>
    )
  }

  render () {
    let sites = _(this.props.sites).sortBy((item) => {
      let field = item[this.state.sortField] || '0'
      if (!isNaN(+field))
        return +field

      return field
    })

    if (this.state.sorting == 'desc')
      sites = sites.reverse()

    sites = sites.value()

    const amountNames = {
      totalCalls: 'Calls',
      impressions: 'Impressions',
      newComments: 'New comments',
      promoComments: 'Promo comments'
    }

    const headerAmounts = _.map(amountNames, (title, fieldName) => {
      return (
        <th className='col-sm-2' key={`metric-site-${fieldName}`}>
          {title}
          &nbsp;
          {this.renderSortIcon(fieldName, 'amount')}
        </th>
      )
    })

    const bodyItems = _.map(sites, (item, i) => {
      return (
        <tr key={`metric-site-${i}`}>
          <td>{i + 1}</td>
          <td>{item['url']}</td>

          {_.map(amountNames, (title, fieldName) => {
            const value = item[fieldName] || '0'

            return (<td key={`metric-site-${fieldName}-${value}`}>{value}</td>)
          })}
        </tr>
      )
    })

    return (
      <Table striped>
        <thead>
        <tr>
          <th className='col-sm-1'>#</th>
          <th className='col-sm-3'>
            Domain
            {this.renderSortIcon('url', 'alpha')}
          </th>
          {headerAmounts}
        </tr>
        </thead>
        <tbody>{bodyItems}</tbody>
      </Table>
    )
  }
}

export default MetricTable