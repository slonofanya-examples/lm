import React from 'react'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import moment from 'moment'
import { Button } from 'react-bootstrap'
import { Icon } from '../helpers/FontAwesome.js'
import diff from 'deep-diff'

class DatePicker extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      from: moment().startOf('month'),
      till: moment()
    }

    this.ranges = {
      'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  }

  updateRange (e, picker) {
    if (e.type !== 'hide') { return }

    const newPeriod = {
      from: picker.startDate,
      till: picker.endDate
    }
    const currPeriod = {
      from: this.state.from,
      till: this.state.till
    }
    const isChanged = diff(currPeriod, { from: newPeriod.from.unix(), till: newPeriod.till.unix() })

    if (!_.isUndefined(isChanged)) {
      this.setState(newPeriod)
      this.props.onChange(newPeriod)
    }
  }

  render () {
    const fromDate = moment(this.state.from).format('YYYY-MM-DD')
    const tillDate = moment(this.state.till).format('YYYY-MM-DD')
    const datePickerLabel = fromDate === tillDate ? fromDate : (`${fromDate} - ${tillDate}`)

    return (
      <DateRangePicker startDate={this.state.from}
                       endDate={this.state.till}
                       ranges={this.ranges}
                       id='metrics-date-range-picker'
                       onEvent={this.updateRange.bind(this)}
      >
        <Button className='selected-date-range-btn' style={{ width: '100%'}}>
          <div className='pull-left'>
            <Icon name='calendar' fw/>
          </div>
          <div className='pull-right'>
            {datePickerLabel}
            <Icon name='caret-down' fw/>
          </div>
        </Button>
      </DateRangePicker>
    )
  }
}

export default DatePicker