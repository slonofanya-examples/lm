import React from 'react'
import UserNavbar from './UserNavbar.js'
import { Link } from 'react-router'

import { Nav, Navbar, CollapsibleNav, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

class TopNavbar extends React.Component {
  render () {
    return (
      <Navbar fixedtop fluid>
        <div className='navbar-header'>
          <span className='navbar-brand'>
            <Link to='/'>Live monitor</Link>
          </span>
        </div>

        <Navbar.Collapse eventKey={0}>
          <Nav navbar>
            <LinkContainer to='/app/sites-metrics'>
              <NavItem>Sites metrics</NavItem>
            </LinkContainer>
          </Nav>

          <Nav pullRight>
            <UserNavbar />
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default TopNavbar
