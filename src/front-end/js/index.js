import 'font-awesome/scss/font-awesome.scss'
import 'index.scss'

import React from 'react'
import {render} from 'react-dom'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import createHashHistory from 'history/lib/createHashHistory'
import createLocation from 'history/lib/createLocation'
import qs from 'qs'
import $ from 'jquery'

import initStore from './store.js'
import initRouter from './router.js'

const history = createHashHistory()
const search = document.location.search
const query = search && qs.parse(search)

$(() => {
  const store = initStore()
  initRouter(history, store)
    .then(({content}) => render(content, document.getElementById('app-container')))
    .then(() => {
      if (!__DEVELOPMENT__) {
        delete window.INITIAL_STATE
      }
    })
})
