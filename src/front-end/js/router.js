import React from 'react'
import Router from 'react-router'
import { Provider } from 'react-redux'

import RouterContainer from './containers/RouterContainer'
import createRoutes from './routes'

export default (history, store) => {
  return new Promise((resolve, reject) => {
    const routes = createRoutes(store)
    const content = (
      <Provider store={store} key='provider'>
        <RouterContainer routes={routes} history={history}/>
      </Provider>
    )

    resolve({content})
  })
}
