import { handleActions } from 'redux-actions'
import _ from 'lodash'
import constants from '../../constants.js'
import $ from 'jquery'
import notify from '../../helpers/notify.js'

const { FETCH_SITES_RATINGS_STATE } = constants.reports.sitesRatings

function fetchStateRequest (state, action) {
  return Object.assign({}, state, {
    isFetching: true
  })
}

function fetchStateSuccess (state, action) {
  let newState = Object.assign({}, state, action.payload, {
    isFetching: false
  })

  return newState
}

function fetchStateError (state, action) {
  notify.error(action.payload)

  return Object.assign({}, state, {
    isFetching: false
  })
}
/**
 * Checking only state keys - not sites length
 * @param state
 * @returns {boolean}
 */
export function isFetched (state) {
  return _.keys(state).length > 1 && !state.isFetching
}

export function isPeriodFetched (state) {
  return _.keys(state).length > 1 && state.period.from > 0
}

/**
 * Applies action.payload=={from, till} to state.period
 * @param state
 * @param action
 * @returns {*}
 */
function setPeriod (state, action) {
  let newState = _.merge({}, state, { period: action.payload })
  newState.sites = []
  return newState
}

export default () => {
  let initialState = {}
  if (__CLIENT__) {
    initialState = _.result(window, 'INITIAL_STATE.reports.sitesRatings')
  } else {
    initialState = _.result(JSON.parse(INITIAL_STATE), 'reports.sitesRatings')
  }

  return handleActions({
    [FETCH_SITES_RATINGS_STATE.REQUEST]: fetchStateRequest,
    [FETCH_SITES_RATINGS_STATE.SUCCESS]: fetchStateSuccess,
    [FETCH_SITES_RATINGS_STATE.ERROR]: fetchStateError
  }, initialState)
}