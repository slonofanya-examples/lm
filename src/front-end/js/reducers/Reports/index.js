import { combineReducers } from 'redux'
import createSitesRatingsReducer from './SitesRatingsReducer'
import createSitesMetricsReducer from './SitesMetricsReducer'

export default () => {
  return combineReducers({
    sitesRatings: createSitesRatingsReducer(),
    sitesMetrics: createSitesMetricsReducer(),
  })
}