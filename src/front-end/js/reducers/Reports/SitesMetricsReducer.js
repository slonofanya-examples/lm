import { handleActions } from 'redux-actions'
import _ from 'lodash'
import constants from '../../constants.js'
import $ from 'jquery'
import notify from '../../helpers/notify.js'

const { FETCH_SITES_METRICS_STATE } = constants.reports.sitesMetrics


function fetchStateRequest (state) {
  return Object.assign({}, state, {
    isFetching: true
  })
}

function fetchStateSuccess (state, action) {
  return Object.assign({},
    state,
    action.payload,
    { isFetching: false, }
  )
}

function fetchStateError (state, action) {
  notify.error(action.payload)

  return Object.assign({}, state, {
    isFetching: false
  })
}
/**
 * Checking only state keys - not sites length
 * @param state
 * @returns {boolean}
 */
export function isFetched (state) {
  return _.keys(state).length > 1 && !state.isFetching
}

export function isPeriodFetched (state) {
  return _.keys(state).length > 1 && state.period.from > 0
}

export default () => {
  let initialState = {}
  if (__CLIENT__) {
    initialState = _.result(window, 'INITIAL_STATE.reports.sitesMetrics')
  } else {
    initialState = _.result(JSON.parse(INITIAL_STATE), 'reports.sitesMetrics')
  }

  return handleActions({
    [FETCH_SITES_METRICS_STATE.REQUEST]: fetchStateRequest,
    [FETCH_SITES_METRICS_STATE.SUCCESS]: fetchStateSuccess,
    [FETCH_SITES_METRICS_STATE.ERROR]: fetchStateError
  }, initialState)
}