import { combineReducers } from 'redux'
import createAppReducer from './AppReducer'
import createReportsReducer from './reports'
import modals from './ModalsReducer'

export default () => {
  return combineReducers({
    application: createAppReducer(),
    reports: createReportsReducer(),
    modals
  })
}
