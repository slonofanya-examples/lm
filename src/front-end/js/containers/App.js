import React from 'react'
import { connect } from 'react-redux'
import { PropTypes } from 'react'
import ModalsContainer from './ModalsContainer.js'
import * as AppActions from '../actions/AppActions.js'
import { isFetched } from '../reducers/AppReducer.js'
import { bindActionCreators } from 'redux'

function select (state) {
  return { application: state.application }
}

function actions (dispatch) {
  return {
    actions: {
      rememberHistory: bindActionCreators(AppActions.rememberHistory, dispatch),
      fetchState: bindActionCreators(AppActions.fetchState, dispatch)
    }
  }
}

@connect(select, actions)
export default class App extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    actions: PropTypes.object
  }

  static fetchState (store) {
    if (isFetched(store.getState().application)) { return Promise.resolve() }

    return Promise.all([store.dispatch(AppActions.fetchState())])
  }

  componentDidMount () {
    this.props.actions.rememberHistory(this.props.history)

    if (isFetched(this.props.application)) { return Promise.resolve() }

    return Promise.all([this.props.actions.fetchState()])
  }

  render () {
    return (
      <div id='app'>
        {this.props.children}
        <ModalsContainer />
      </div>
    )
  }
}
