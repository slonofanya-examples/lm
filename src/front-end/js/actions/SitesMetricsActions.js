import notify from '../helpers/notify.js'
import request from 'superagent'
import _ from 'lodash'
import moment from 'moment'
import { createAction } from 'redux-actions'
import constants from '../constants.js'
import 'isomorphic-fetch'

const { FETCH_SITES_METRICS_STATE } = constants.reports.sitesMetrics
const { FETCH_SITES_RATINGS_STATE } = constants.reports.sitesRatings

function fetchMetrics (period = null) {
  let query = ''
  if (period) {
    query = `?from=${moment(period.from).unix()}&till=${moment(period.till).unix()}`
  }

  return {
    type: FETCH_SITES_METRICS_STATE,
    payload: `/metrics${query}`
  }
}

function fetchRatings (period = null) {
  let query = ''
  if (period) {
    query = `?from=${moment(period.from).unix()}&till=${moment(period.till).unix()}`
  }

  return {
    type: FETCH_SITES_RATINGS_STATE,
    payload: `/reports/ratings${query}`
  }
}

export default {
  fetchMetrics,
  fetchRatings,
}