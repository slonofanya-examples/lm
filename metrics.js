'use strict'

require('./lib/fs-loader')
const _ = require('lodash')
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (_.include(['development', 'stage'], process.env.NODE_ENV)) {
  process.env.LOG_LEVEL = 'info'
} else if (process.env.NODE_ENV === 'test') {
  process.env.LOG_LEVEL = 'info'
} else {
  process.env.LOG_LEVEL = 'err'
}

const requireCfg = require('./lib/requireCfg')
var serverCfg = requireCfg('metrics')

const initMongo = require('./lib/initializers/mongo')
const initCouchbase = require('./lib/initializers/couchbase')
const initExpress = require('./lib/initializers/express-metrics')
const http = require('http')
const log = require('./lib/logger').get('Metrics')

log.info(`Starting app in [${process.env.NODE_ENV}] mode`)

const port = serverCfg.port || 3000
let onServerListenHook = null
let server

function onError (err) {
  if (err.syscall !== 'listen') {
    throw err
  }

  var bind = 'Port ' + port

  switch (err.code) {
    case 'EACCES':
      log.error(`${bind} requires elevated privileges`)
      process.exit(1)
      break
    case 'EADDRINUSE':
      log.error(`${bind} is already in use`)
      process.exit(1)
      break
    default:
      throw err
  }
}

function onListening () {
  const addr = server.address()
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port
  log.info(`Listening on ${bind}`)
  if (_.isFunction(onServerListenHook)) {
    onServerListenHook(null, server)
  }
}

function onClosing () {
  log.info('Server stopped')
}

Promise.all([
    initMongo(),
    initCouchbase(),
    initExpress(),
  ])
  .then((res) => {
    const app = res[2]
    server = http.createServer(app)
    server.listen(port)
    server.on('error', onError)
    server.on('listening', onListening)
    server.on('close', onClosing)
  })
  .catch(onError)

module.exports = (hook) => {
  if (_.isFunction(hook)) {
    onServerListenHook = hook
  }
}
