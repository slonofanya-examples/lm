'use strict'

const $require = require(`${process.cwd()}/lib/require`)
const _ = require('lodash')
const bluebird = require('bluebird')
const redis = bluebird.promisifyAll(require('redis'))

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (_.include(['development', 'stage'], process.env.NODE_ENV)) {
  process.env.LOG_LEVEL = 'info'
} else if (process.env.NODE_ENV === 'test') {
  process.env.LOG_LEVEL = 'info'
} else {
  process.env.LOG_LEVEL = 'err'
}

const initMongo = $require('lib/initializers/mongo')
const initRedis = $require('lib/initializers/redis')
const initCouchbase = $require('lib/initializers/couchbase')
const log = $require('lib/logger').get('Worker')
const workerCfg = require('konphyg')(`${process.cwd()}/config`)('worker')

let redisClient = null
let couchClient = null

function onError (err) {
  if (err.syscall !== 'listen') {
    throw err
  }

  var bind = 'Port ' + port

  switch (err.code) {
    case 'EACCES':
      log.error(`${bind} requires elevated privileges`)
      process.exit(1)
      break
    case 'EADDRINUSE':
      log.error(`${bind} is already in use`)
      process.exit(1)
      break
    default:
      throw err
  }
}

const tasks = $require('lib/worker-tasks')

function pollingPromiseQueue () {
  log.info('Start polling...')

  return tasks.updateSiteMetrics(redisClient)
    .then(() => tasks.updateSiteStatistics(redisClient, couchClient))
    .then(() => {
      log.info('End polling.')
    })
    .catch(log.error)
}

function initPolling () {
  log.info(`Starting app in [${process.env.NODE_ENV}] mode`)
  log.info(`Polling interval is [${workerCfg.pollingInterval / 1000}] sec`)

  Promise.all([
      initRedis(),
      initMongo(),
      initCouchbase(),
    ])
    .then((res) => {
      redisClient = res[0]
      couchClient = res[2]

      pollingPromiseQueue()
        .then(() => {
          setInterval(pollingPromiseQueue, workerCfg.pollingInterval)
        })
        .catch(log.error)
    })
    .catch(onError)
}

if (process.env.NODE_ENV !== 'test') {
  initPolling()
}