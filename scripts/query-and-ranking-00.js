'use strict'
const _ = require('lodash')
const moment = require('moment')
const bluebird = require('bluebird')
const mongoose = require('mongoose')

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (_.include(['development', 'stage'], process.env.NODE_ENV)) {
  process.env.LOG_LEVEL =  'info'
} else if (process.env.NODE_ENV === 'test') {
  process.env.LOG_LEVEL =  'info'
} else {
  process.env.LOG_LEVEL =  'err'
}

const initMongo = require('../lib/initializers/mongo')
const log = require('../lib/logger').get('ranking-test')
const mongoCfg = require('konphyg')(`${process.cwd()}/config`)('mongo')

const Site = require('../models/site')
const SiteStat = require('../models/siteStat')

function queryAndAggregate () {
  return new Promise ((resolve, reject) => {
    const startDate = moment('2015-09-02').valueOf()
    const stopDate = moment('2015-09-12').valueOf()
    SiteStat.aggregate([
      { $match: {
          timestamp : {
            $gte: startDate,
            $lte: stopDate,
          }
        }
      },
      {
        $group: {
          _id: '$_creator',
          total: { $sum: '$calls' }
        }
      }
    ], (err, res) => {
      if (err) { return reject(err) }

      resolve(res)
    })
  })
}

initMongo()
  .tap(() => { console.time('aggregate') })
  .then(queryAndAggregate)
  .then((res) => {
    log.info(`received aggregated info for [${res.length}] sites`)
    log.info('aggregation done!')
    console.timeEnd('aggregate')

    process.exit(0)
  })
  .catch((err) => {
    log.error(err)
    process.exit(1)
  })