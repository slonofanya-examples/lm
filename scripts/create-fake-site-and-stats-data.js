'use strict'
const faker = require('faker')
const _ = require('lodash')
const moment = require('moment')
const bluebird = require('bluebird')
const mongoose = require('mongoose')

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (_.include(['development', 'stage'], process.env.NODE_ENV)) {
  process.env.LOG_LEVEL =  'info'
} else if (process.env.NODE_ENV === 'test') {
  process.env.LOG_LEVEL =  'info'
} else {
  process.env.LOG_LEVEL =  'err'
}

const initMongo = require('../lib/initializers/mongo')
const log = require('../lib/logger').get('faker')
const mongoCfg = require('konphyg')(`${process.cwd()}/config`)('mongo')

const Site = require('../models/site')
const SiteStat = require('../models/siteStat')

function cleanDB () {
  return new Promise((resolve) => {
    log.info(`Gonna drop [${_.last(mongoCfg.uri.split('/'))}] DB`)
    mongoose.connection.db.dropDatabase(() => {
      resolve(true)
    })
  })
}

function fakeSiteGen () {
  let id = 0
  return () => {
    const domain = faker.internet.domainName()
    return {
      name : faker.lorem.sentence(),
      shortname : `${domain.replace('.', '-')}-${id++}`,
      url: `http://www.${domain}`
    }
  }
}

function getSiteStatsPromise (site, timestamp) {
  return new Promise((resolve, reject) => {
    const payload = {
      _creator: site._id,
      timestamp: timestamp,
      calls: _.random(0, 1080)
    }

    SiteStat.create(payload, (err, stat) => {
      if (err) { return reject(err) }
      resolve(stat)
    })
  })
}

/**
 * Generates fake stats data for every site for period == last month
 * @param sites - array of models
 * @returns {Promise}
 */
function genSitesStats (sites) {
  const fromDate = moment().startOf('month')
  const tillDate = moment().endOf('month')
  const days = tillDate.date()
  const pollingInterval = 60 //min
  const statsAmount = parseInt(days * ( 24*60/pollingInterval ))

  log.info('Prepare promises ..')
  console.time('prepare stats promises')
  let statsPromises = []
  sites.forEach((site) => {
    let date = moment(fromDate)
    statsPromises.push(_.times(statsAmount, (idx) => {
      const timestamp = date.add(pollingInterval, 'minutes').valueOf()
      return getSiteStatsPromise(site, timestamp)
    }))
  })

  statsPromises = _.flatten(statsPromises)
  console.timeEnd('prepare stats promises')
  log.info(`Gonna resolve [${statsPromises.length}] promises ...`)
  console.time('stats promises')
  return Promise.all(statsPromises)
}

function fillDB () {
  const sitesAmount = 108

  function getCreateSitePromise () {
    return new Promise((resolve, reject) => {
      const payload = siteGen()
      Site.create(payload, (err, site) => {
        if (err) { return reject(err) }

        resolve(site)
      })
    })
  }

  let siteGen = fakeSiteGen()
  const sitesPromises = _.times(sitesAmount, getCreateSitePromise)

  log.info('Filling DB with fake sites ...')
  console.time('sites')
  return bluebird.all(sitesPromises)
    .tap((sites) => {
      log.info(`Created [${sites.length}] fake sites`)
      console.timeEnd('sites')
    })
    .then(genSitesStats)
    .tap((siteStats) => {
      log.info(`Created [${siteStats.length}] stats records`)
      console.timeEnd('stats promises')
    })
}

initMongo()
  //.then(cleanDB)
  .then(fillDB)
  .then(() => {
    log.info('done!')
    process.exit(0)
  })
  .catch((err) => {
    log.error(err)
    process.exit(1)
  })