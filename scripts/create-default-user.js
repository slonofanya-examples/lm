'use strict'
const _ = require('lodash')
const bluebird = require('bluebird')
const mongoose = require('mongoose')

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (_.include(['development', 'stage'], process.env.NODE_ENV)) {
  process.env.LOG_LEVEL = 'info'
} else if (process.env.NODE_ENV === 'test') {
  process.env.LOG_LEVEL = 'info'
} else {
  process.env.LOG_LEVEL = 'err'
}

const initMongo = require('../lib/initializers/mongo')
const log = require('../lib/logger').get('Default user')

const User = require('../models/user')

function createUser () {
  const deferred = bluebird.defer()
  User.find().count((err, res) => {
    if (err) { return deferred.reject(err) }

    if (res !== 0) {
      log.warn('User already created. Quiting')
      return deferred.resolve()
    }

    log.info('Gonna create default user ...')
    const data = {
      firstName: 'Live',
      lastName: 'Monitor',
      email: 'live-monitor',
      password: '3ct47my3c4k!@#'
    }

    User.create(data, (err, user) => {
      if (err) { return deferred.reject(err) }

      log.info(`User credentials for login:\n email: ${data.email}\n password: ${data.password}`)
      deferred.resolve()
    })
  })

  return deferred.promise
}

initMongo()
  .then(createUser)
  .then(() => {
    log.info('done!')
    process.exit(0)
  })
  .catch(err => {
    log.error(err)
    process.exit(1)
  })
