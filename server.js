'use strict'
const _ = require('lodash')
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (_.include(['development', 'stage'], process.env.NODE_ENV)) {
  process.env.LOG_LEVEL = 'info'
} else if (process.env.NODE_ENV === 'test') {
  process.env.LOG_LEVEL = 'info'
} else {
  process.env.LOG_LEVEL = 'err'
}

var serverCfg = require('konphyg')(`${process.cwd()}/config`)('server')

global.__CLIENT__ = false
global.__SERVER__ = true
global.__DEVELOPMENT__ = process.env.NODE_ENV === 'development'
global.__API_BASE_URL__ = `http://${serverCfg.host}:${serverCfg.port}${serverCfg.api.mountPoint}`
global.INITIAL_STATE = {}

const initMongo = require('./lib/initializers/mongo')
const initCouchbase = require('./lib/initializers/couchbase')
const initExpress = require('./lib/initializers/express')
const http = require('http')
const log = require('./lib/logger').get()

log.info(`Starting app in [${process.env.NODE_ENV}] mode`)

const port = serverCfg.port || 3000
let onServerListenHook = null
let server

function onError (err) {
  if (err.syscall !== 'listen') {
    throw err
  }

  var bind = 'Port ' + port

  switch (err.code) {
    case 'EACCES':
      log.error(`${bind} requires elevated privileges`)
      process.exit(1)
      break
    case 'EADDRINUSE':
      log.error(`${bind} is already in use`)
      process.exit(1)
      break
    default:
      throw err
  }
}

function onListening () {
  const addr = server.address()
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port
  log.info(`Listening on ${bind}`)
  if (_.isFunction(onServerListenHook)) {
    onServerListenHook(null, server)
  }
}

function onClosing () {
  log.info('Server stopped')
}

Promise.all([
    initMongo(),
    initCouchbase(),
    initExpress(),
  ])
  .then((res) => {
    const app = res[2]
    server = http.createServer(app)
    server.listen(port)
    server.on('error', onError)
    server.on('listening', onListening)
    server.on('close', onClosing)
  })
  .catch(onError)

module.exports = (hook) => {
  if (_.isFunction(hook)) {
    onServerListenHook = hook
  }
}
