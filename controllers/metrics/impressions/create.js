'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const Metric = $require('models/metric')
const errResBadRequest = $require('lib/api-err-responders/bad-request')
const metricsConfig = require('konphyg')(process.cwd() + '/config')('metrics')
const _ = require('lodash')

const allowedParams = ['siteShortName', 'value']

module.exports = (req, res, next) => {
  const notAllowedParams = _.chain(req.body).keys().difference(allowedParams).value()

  if (notAllowedParams.length > 0) {
    return next(errResBadRequest(`Request params not allowed: [${notAllowedParams.join(', ')}]`))
  }

  if (!req.body.siteShortName) {
    return next(errResBadRequest('siteShortName is required'))
  }

  const payload = {
    siteShortName: req.body.siteShortName,
    value: _.result(req, 'body.value'),
    metricType: metricsConfig.types.impressions
  }

  return Metric.create(payload)
    .then(() => {
      res.body = { saved: 'ok' }
      next()
    })
    .catch(next)
}