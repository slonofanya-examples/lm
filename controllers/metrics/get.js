'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const Metric = $require('models/metric')
const Site = $require('models/site')
const errResBadRequest = $require('lib/api-err-responders/bad-request')
const metricsConfig = require('konphyg')(process.cwd() + '/config')('metrics')
const _ = require('lodash')
const moment = require('moment')

const allowedParams = ['from', 'till']

function queryAndAggregate (period) {
  return new Promise((resolve, reject) => {
    Metric.aggregate([{
      $match: {
        timestamp: {
          $gte: period.from,
          $lte: period.till,
        }
      }
    }, {
      $group: {
        _id: {site: '$site', metricType: '$metricType'},
        value: { $sum: '$value' },
      },
    }], (err, metrics) => {
      if (err) { return reject(err) }
      resolve(metrics)
    })
  })
}

function aggregateValues(metrics) {
  return site => {
    let metric = site.toJSON()

    _.forEach(metricsConfig.types, (metricType, metricTypeName) => {
      const impression = _.findWhere(metrics, { _id: { site: site._id, metricType: metricType } })

      metric[metricTypeName] = impression ? impression.value : 0
    })

    return metric
  }
}

function populateSites (metrics) {
  return new Promise((resolve, reject) => {
    if (!metrics.length) { resolve([]) }

    const ids = _.pluck(metrics, '_id.site')

    Site.find({ _id: { $in: ids } }, (err, sites) => {
      if (err) { return reject(err) }

      sites = _.map(sites, aggregateValues(metrics))

      resolve(sites)
    })
  })
}

module.exports = (req, res, next) => {
  const notAllowedParams = _.chain(req.body).keys().difference(allowedParams).value()

  if (notAllowedParams.length > 0) {
    return next(errResBadRequest(`Request params not allowed: [${notAllowedParams.join(', ')}]`))
  }

  const period = _.defaults(_.pick(req.query, ['from', 'till']), {
    from: moment().startOf('month').unix(),
    till: moment().unix()
  })
  const endOfDay = moment().endOf('day').unix()

  period.from = parseInt(period.from)
  period.till = Math.min(parseInt(period.till), endOfDay)

  if (period.from > period.till) {
    return next(errResBadRequest('`from` date can\'t be after `till` date'))
  }

  return queryAndAggregate(period)
    .then(populateSites)
    .then(data => {
      res.body = {
        period: period,
        sites: data
      }
      next()
    })
    .catch(next)
}