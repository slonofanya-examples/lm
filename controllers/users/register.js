'use strict'
var User = require('../../models/user')

module.exports = (req, res, next) => {
  function onRejected (err) {
    next(err)
  }

  function onFullfiled (user) {
    res.body = user.toJSON()
    next()
  }

  User.create(req.body).then(onFullfiled, onRejected)
}
