'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const _ = require('lodash')
const moment = require('moment')
const bluebird = require('bluebird')
const errResBadRequest = $require('lib/api-err-responders/bad-request')

const Site = $require('models/site')
const SiteStat = $require('models/siteStat')

function queryAndAggregate (period) {
  const deferred = bluebird.defer()

  SiteStat.aggregate([
    { $match: {
      timestamp : {
        $gte: period.from,
        $lte: period.till,
      }
    }
    },
    {
      $group: {
        _id: '$_creator',
        total: { $sum: '$calls' }
      }
    }
  ], (err, res) => {
    if (err) { return deferred.reject(err) }
    deferred.resolve(res)
  })

  return deferred.promise
}

function populateSites (stats) {
  const deferred = bluebird.defer()

  const ids = _.pluck(stats, '_id')

  Site.find({ _id: { $in: ids} }, (err, sites) => {
    if (err) { return deferred.reject(err) }

    const res = _.map(sites, (site) => {
      const stat = _.findWhere(stats, { _id: site._id})
      const siteJSON = site.toJSON()
      siteJSON.totalCalls = stat.total

      return siteJSON
    })

    deferred.resolve(res)
  })

  return deferred.promise
}

module.exports = (req, res, next) => {
  const period = _.defaults(_.pick(req.query, ['from', 'till']), {
    from: moment().startOf('month').valueOf(),
    till: moment().valueOf()
  })

  period.from = parseInt(period.from)
  period.till = parseInt(period.till)

  if (moment(period.till).valueOf() > moment().endOf('day').valueOf()) {
    period.till = moment().valueOf()
  }

  if (moment(period.from).valueOf() > moment(period.till).valueOf()) {
    return next(errResBadRequest('`from` date can\'t be after `till` date'))
  }

  queryAndAggregate(period)
    .then(populateSites)
    .then((data) => {
      res.body = {
        period: period,
        sites: data
      }
      next()
    })
    .catch((err) => next(err))
}