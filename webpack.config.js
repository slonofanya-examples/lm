var webpack = require('webpack')
var path = require('path')
var _ = require('lodash')
var srcPath = path.join(__dirname, 'src')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var config = require('konphyg')(process.cwd() + '/config')
var serverCfg = config('server')
var frontCfg = config('front-end')

var port = frontCfg.port ? ':' + frontCfg.port : ''
var apiBaseUrl = 'http://' + frontCfg.host + port + serverCfg.api.mountPoint

module.exports = {
  target: 'web',
  cache: true,
  entry: {
    'index.bundle': path.join(srcPath, 'front-end/js/index.js'),
    'common.bundle': [
      'jquery', 'bootstrap', 'bootstrap-notify',
      'lodash', 'moment', 'superagent',
      'react', 'react-dom', 'react-router', 'history', 'react-bootstrap', 'react-router-bootstrap',
      'react-addons-create-fragment', 'react-bootstrap-daterangepicker',
      'redux', 'react-redux', 'redux-actions', 'js-cookie', 'lodash', 'marked'
    ],
    'vendor.bundle': path.join(srcPath, 'vendor/js/index.js')
  },
  resolve: {
    root: srcPath,
    extensions: ['', '.js', '.jsx', '.scss'],
    modulesDirectories: ['node_modules',
      'src', 'src/front-end/styles',
      'src/vendor/js/jquery-plugins']
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name].js',
    pathInfo: true
  },

  module: {
    loaders: [
      { test: /(\.js|\.jsx)$/, exclude: /node_modules/, loader: 'babel?cacheDirectory' },
      {
        test: /\.scss/,
        loader: ExtractTextPlugin.extract('style', 'css!sass')
      },
      {
        test: /\.css/,
        loader: ExtractTextPlugin.extract('style', 'css!cssnext')
      },
      {
        test: /\.less/,
        loader: ExtractTextPlugin.extract('style', 'css!less')
      },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url?limit=10000&minetype=application/font-woff' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file' },

      { test: /\.jpe?g$|\.gif$|\.png$|\.wav$|\.mp3$/, loader: 'file' }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      __FETCH_STATE__: true,
      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: _.include(['development', 'stage'], process.env.NODE_ENV),
      __API_BASE_URL__: JSON.stringify(apiBaseUrl)
    }),
    new webpack.optimize.CommonsChunkPlugin('common.bundle', 'common.bundle.js'),
    new webpack.SourceMapDevToolPlugin({
      filename: '[name].js.map',
      exclude: [/\.css/]
    }),
    new ExtractTextPlugin('[name].css', { allChunks: true }),
    new webpack.ProvidePlugin({
      jQuery: "jquery"
    })
  ],
  debug: true,
  cssnext: {
    browsers: 'last 2 versions'
  }
}
