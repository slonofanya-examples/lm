'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const getAllMetricsCtrl = $require('controllers/metrics/get')
const express = require('express')

let router = new express.Router()

router.get('/metrics', getAllMetricsCtrl)

module.exports = { router }