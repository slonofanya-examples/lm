'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const ratingsCtrl = $require('controllers/reports/ratings')
const express = require('express')

let router = new express.Router()

router.get('/ratings', ratingsCtrl)

module.exports = { router: router }
