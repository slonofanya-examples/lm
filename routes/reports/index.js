'use strict'
const express = require('express')
const requireTree = require('require-tree')
const apiHelpers = requireTree('../../lib/api-helpers')

const serverCfg = require('konphyg')(`${process.cwd()}/config`)('server')

const ratings = require('./ratings')

let router = new express.Router()

router.use('/reports', [ apiHelpers.allowLogged, ratings.router ])

module.exports = { router: router }