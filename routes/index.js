'use strict'
const _ = require('lodash')
const express = require('express')
const requireCfg = require('../lib/requireCfg')
let router = new express.Router()
let apiRouter = new express.Router()

const serverCfg = requireCfg('server')

const jwt = require('./auth/jwt')
const users = require('./users')
const reports = require('./reports')
const metrics = require('./metrics')

const pkg = require(`${process.cwd()}/package.json`)

router.use(jwt.router)

router.get('/', (req,res) => {
  let initialState = {
    application: {
      token: null,
      user: null,
      nextTransitionPath: req.query.targetPath || ''
    },
    reports: {
      sitesRatings : {},
      sitesMetrics : {},
    }
  }

  if (_.result(req, 'session.user')) {
    initialState.application = _.merge(initialState.application, {
      token: req.cookies.token,
      user: req.session.user
    })
  }

  var opts = {
    title: pkg.name,
    author: pkg.author,
    description: pkg.description,
    version: pkg.version,
    keywords: pkg.keywords.join(', '),
    html: null,
    initialState: JSON.stringify(initialState)
  }

  res.render('index', opts)
})

apiRouter.use(users.router)
apiRouter.use(reports.router)
apiRouter.use(metrics.router)

router.use(serverCfg.api.mountPoint, apiRouter)

module.exports = { router: router }