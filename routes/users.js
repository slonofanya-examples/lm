'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const requireTree = require('require-tree')
const usersCtrl = requireTree('../controllers/users')
const apiHelpers = requireTree('../lib/api-helpers')
const express = require('express')

let User = $require('models/user')

let router = new express.Router()

router.get('/users/current', [ apiHelpers.allowLogged, usersCtrl.current ])
router.post('/users/log-in', usersCtrl.logIn)
router.post('/users/log-out', usersCtrl.logOut)
router.post('/users/register', usersCtrl.register)

router.use('/users', apiHelpers.buildCRUD(User, {
  actions: {
    'pre-list': apiHelpers.allowLogged,
    'pre-retrieve': apiHelpers.allowLogged,
    'pre-patch': apiHelpers.allowLogged,
    'pre-remove': apiHelpers.allowLogged,
    update: false
  }
}))

module.exports = { router: router }
