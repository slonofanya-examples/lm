'use strict'
var jwt = require('jsonwebtoken')
var expressJwt = require('express-jwt')
var secretsCfg = require('konphyg')(process.cwd() + '/config')('secrets')
const express = require('express')

let router = new express.Router()

const authorize = expressJwt({
  secret: secretsCfg.jwt.key,
  getToken: (req) => {
    if (req.headers.authorization) {
      return req.headers.authorization
    } else if (req.query && req.query.token) {
      return req.query.token
    } else if (req.cookies && req.cookies.token) {
      return req.cookies.token
    }
    return null
  }
})

router.use((req, res, next) => {
  req.getToken = (user) => {
    var userProfile = {
      id: user.id.toString(),
      created: user.created,
      email: user.email
    }
    return jwt.sign(userProfile, secretsCfg.jwt.key, secretsCfg.jwt.options)
  }

  req.session = {}

  if (req.headers['authorization'] || req.cookies['token']) {
    authorize(req, res, (v) => {
      if (req.user) {
        req.session.userId = req.user.id
        req.session.user = req.user
        delete req.user
      }
      next()
    })
  } else {
    next()
  }
})

module.exports = {
  router: router
}
