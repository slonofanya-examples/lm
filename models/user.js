'use strict'
const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const createdModifiedPlugin = require('mongoose-createdmodified').createdModifiedPlugin
const paginatorPlugin = require('mongoose-paginator')
const bcrypt = require('bcryptjs')
const _ = require('lodash')
const transformToJSON = require('./transforms/tojson')
const errResNotFound = require('../lib/api-err-responders/resource-not-found')
const errWrongCredentials = require('../lib/api-err-responders/wrong-credentials')

const saltRounds = 10
const schema = {
  email: { type: String, required: true, unique: true, lowercase: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  password: { type: String, select: false, required: true },
  active: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false }
}
const userSchema = new mongoose.Schema(schema)

userSchema.plugin(uniqueValidator, { message: '{PATH} already in use' })
userSchema.plugin(createdModifiedPlugin, { index: true })
userSchema.plugin(paginatorPlugin, { defaultKey: '_id', direction: -1, sort: 'created' })
transformToJSON(userSchema, schema)

userSchema.path('email')
  .validate(require('./validators/email'), 'email invalid')

userSchema.pre('save', function (next) {
  const self = this

  if (!this.isModified('password')) {
    return next()
  }

  bcrypt.hash(this.password, saltRounds, (err, passwordHash) => {
    if (err) return next(err)
    self.password = passwordHash
    next()
  })
})

userSchema.static('findByCredentials', function (email, password) {
  const self = this
  return new Promise ((resolve, reject) => {
    let q = self.findOne({
      email: email.toLowerCase(),
      deleted: {$ne: true}
    })
    q.select('+password')
    q.exec((err, user) => {
      if (err) return reject(err)
      if (!user) return reject(errResNotFound(email, 'User'))

      bcrypt.compare(password, user.password, (err, passwordsMatch) => {
        if (err) {
          console.error(err)
          return reject(err) // don't pass forward errors of bcrypt
        }
        if (!passwordsMatch) return reject(errWrongCredentials())

        return resolve(user)
      })
    })

  })
})

userSchema.static('findByEmail', (email, next) => {
  this.findOne({
    email: email
  }, (err, user) => {
    if (err) return next(err)
    if (!user) return next()
    next(null, user)
  })
})

userSchema.static('findByIdAndActivate', (id, body, next) => {
  this.findById(id).select('+password').exec((err, user) => {
    if (err) return next(err)
    if (!user) return next()
    if (_.isUndefined(body.password)) {
      user.password = body.password
    }
    user.active = true
    user.save(function (err) {
      if (err) return next(err)
      next(null, user)
    })
  })
})

userSchema.method('changePassword', (newPassword, next) => {
  this.password = newPassword
  this.save((err, user) => {
    if (err) return next(err)
    next(null, user)
  })
})

module.exports = mongoose.model('User', userSchema)
