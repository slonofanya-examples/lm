'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const couchbase = $require('lib/adapters/couchbase')
const mongoose = require('mongoose')
const ValidationError = mongoose.Error.ValidationError
const ValidatorError = mongoose.Error.ValidatorError
const _ = require('lodash')
const moment = require('moment')
const Site = $require('models/site')
const errResNotFound = $require('lib/api-err-responders/resource-not-found')
const metricsConfig = require('konphyg')(process.cwd() + '/config')('metrics')

const metricSchema = new mongoose.Schema({
  metricType: { type: String, enum: _.values(metricsConfig.types), required: true },
  site: { type: mongoose.Schema.Types.ObjectId, ref: 'Site', required: true },
  value: { type: Number, required: true },
  timestamp: { type: Number, required: true },
})

function getSiteFromCouchbase (siteShortName) {
  return new Promise((resolve, reject) => {
    couchbase.selectBucket('default').get(`site_${siteShortName}`, (err, cbRes) => {
      if (err) {
        return reject(errResNotFound(siteShortName))
      }

      resolve(_.pick(cbRes.value, ['name', 'url', 'shortname']))
    })
  })
}

function createSite (payload) {
  return new Promise((resolve, reject) => {
    Site.create(payload, (err, site) => {
      if (err) return reject(err)

      resolve(site)
    })
  })
}

function getSite (siteShortName) {
  return Site.findByShortName(siteShortName).then(dbSite => {
    if (dbSite)
      return dbSite

    // Site not found
    return getSiteFromCouchbase(siteShortName).then(createSite)
  })
}

function wrongValueError (value) {
  const error = _.extend({}, new ValidationError(), {
    errors: {
      value: new ValidatorError({
        path: 'value',
        message: 'Wrong value format, must be numeric',
        value: value,
      })
    }
  })

  return Promise.reject(error)
}

metricSchema.static('create', function (payload) {
  const value = payload.value ? +payload.value : 1

  if (_.isNaN(value)) { return wrongValueError(value) }

  return getSite(payload.siteShortName)
    .then(dbSite => {
      const where = {
        metricType: payload.metricType,
        site: dbSite._id
      }
      const updateData = {
        $inc: { value: value },
        timestamp: moment().unix()
      }
      const options = { "upsert": true, "new": true }

      return this.findOneAndUpdate(where, updateData, options)
    })
})

module.exports = mongoose.model('Metric', metricSchema)