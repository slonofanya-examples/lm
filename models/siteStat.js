'use strict'
const mongoose = require('mongoose')
const paginatorPlugin = require('mongoose-paginator')
const _ = require('lodash')
const transformToJSON = require('./transforms/tojson')

const schema = {
  _creator: { type: mongoose.Schema.Types.ObjectId, ref: 'Site' },
  calls: { type: Number, required: true },
  timestamp: { type: Number, required: true },
  deleted: { type: Boolean, default: false }
}
const siteStatSchema = new mongoose.Schema(schema, { capped: 1000000000 })

siteStatSchema.plugin(paginatorPlugin, { defaultKey: '_id', direction: -1, sort: 'created' })
transformToJSON(siteStatSchema, schema)

module.exports = mongoose.model('SiteStat', siteStatSchema)
