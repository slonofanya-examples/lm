'use strict'
const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const createdModifiedPlugin = require('mongoose-createdmodified').createdModifiedPlugin
const paginatorPlugin = require('mongoose-paginator')
const _ = require('lodash')
const transformToJSON = require('./transforms/tojson')

const schema = {
  name: { type: String, required: true },
  shortname: { type: String, required: true, unique: true },
  url: { type: String, required: true },
  deleted: { type: Boolean, default: false }
}
const siteSchema = new mongoose.Schema(schema)

siteSchema.plugin(uniqueValidator, { message: '{PATH} already in use' })
siteSchema.plugin(createdModifiedPlugin, { index: true })
siteSchema.plugin(paginatorPlugin, { defaultKey: '_id', direction: -1, sort: 'created' })
transformToJSON(siteSchema, schema)

siteSchema.static('findByShortName', function (name) {
  return new Promise((resolve, reject) => {
    this.findOne({ shortname: name }, 'name, shortname url', (err, site) => {
      if (err) return reject(err)

      resolve(site)
    })
  })
})

module.exports = mongoose.model('Site', siteSchema)
