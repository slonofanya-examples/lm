/* global before, after, it, expect, faker, describe, helpers */
'use strict'
var request = require('superagent')

describe('log out', function () {
  const userData = {
    email: faker.internet.email(),
    password: faker.internet.password(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName()
  }

  const testUrl = helpers.variables.apiEndpoint + '/users/log-out'

  before(helpers.start)
  before((next) => {
    helpers.user.createAndLogin(userData).then((user) => {
      next()
    })
  })
  after(helpers.stopServer)

  it('log out user', (next) => {
    request.post(testUrl)
      .send(userData)
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.statusCode).to.be.equal(200)
        var body = res.body
        expect(body).to.be.deep.equal({success: true})
        next()
      })
  })
})
