/* global before, after, it, expect, faker, describe, helpers */
'use strict'
const request = require('superagent')
const _ = require('lodash')

describe('[EDGE-CASES] register', () => {
  const userData = {
    email: faker.internet.email(),
    password: faker.internet.password(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName()
  }

  const testUrl = helpers.variables.apiEndpoint + '/users/register'

  before(helpers.start)
  after(helpers.stopServer)

  it('don\'t register new user with wrong details', (next) => {
    let currUserData = _.clone(userData)
    delete currUserData.lastName
    const expectedRes = {
      errors: {
        lastName: ['Path `lastName` is required.']
      }
    }

    request.post(testUrl)
      .send(currUserData)
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(err).to.be.instanceof(Error)
        expect(res.statusCode).to.be.equal(422)
        expect(res.body).to.be.deep.equal(expectedRes)
        next()
      })
  })
})
