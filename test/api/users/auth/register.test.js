/* global before, after, it, expect, faker, describe, helpers */
'use strict'
const request = require('superagent')

describe('register', function () {
  const userData = {
    email: faker.internet.email(),
    password: faker.internet.password(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName()
  }

  const testUrl = helpers.variables.apiEndpoint + '/users/register'

  before(helpers.start)
  after(helpers.stopServer)

  it('register new user', function (next) {
    request.post(testUrl)
      .send(userData)
      .set('Accept', 'application/json')
      .end(function (err, res) {
        expect(err).to.be.null
        expect(res.statusCode).to.be.equal(200)
        var body = res.body
        expect(body.email).to.be.equal(userData.email.toLowerCase())
        expect(body.password).to.be.undefined
        expect(body.token).to.be.undefined
        next()
      })
  })
})
