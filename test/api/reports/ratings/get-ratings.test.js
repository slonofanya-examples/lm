/* global before, after, it, expect, faker, describe, helpers */
'use strict'
var request = require('superagent')
var moment = require('moment')

describe('reports/ratings', function () {
  const testUrl = helpers.variables.apiEndpoint + '/reports/ratings'
  let currUser

  before(helpers.start)
  before((next) => {
    const userData = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName()
    }

    helpers.user.createAndLogin(userData).then((user) => {
      currUser = user
      next()
    })
    .catch((err) => {
        next(err)
    })
  })
  after(helpers.stopServer)

  it('should use curr month for period if not provided', (next) => {
    const expected = {
      period: {
        from: moment().startOf('month').valueOf(),
        till: moment().startOf('day').valueOf()
      }
    }

    request.get(testUrl)
      .set({ Authorization: currUser.token })
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.statusCode).to.be.equal(200)
        var body = res.body
        expect(body).to.be.deep.equal(expected)
        next()
      })
  })

  it('should return array of sites rating for last month period', (next) => {
    const from = moment().subtract(1, 'month').startOf('month').valueOf()
    const till = moment().subtract(1, 'month').endOf('month').valueOf()
    const url = `${testUrl}?from=${from}&till=${till}`

    const expectedPeriod = {
      from: from,
      till: till
    }

    helpers.ratings.genFake()
      .then(() => {
        console.time('request')
        request.get(url)
          .set({ Authorization: currUser.token })
          .end((err, res) => {
            console.timeEnd('request')
            expect(err).to.be.null
            expect(res.statusCode).to.be.equal(200)

            var body = res.body
            expect(body.period).to.be.deep.equal(expectedPeriod)
            expect(body.sites).to.be.an('array')
            body.sites.forEach((site) => {
              expect(site).to.have.property('totalCalls')
              expect(site.totalCalls).to.be.an('number')
            })

            next()
          })
      })
  })
})
