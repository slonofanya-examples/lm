/* global before, after, it, expect, faker, describe, helpers */
'use strict'
const request = require('superagent')
const moment = require('moment')

describe('[EDGE CASES] reports/ratings', function () {
  const testUrl = helpers.variables.apiEndpoint + '/reports/ratings'
  let currUser

  before(helpers.start)
  before((next) => {
    const userData = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName()
    }

    helpers.user.createAndLogin(userData).then((user) => {
      currUser = user
      next()
    })
      .catch((err) => {
        next(err)
      })
  })
  after(helpers.stopServer)

  it('return err for NOT logged user', (next) => {
    request.get(testUrl)
      .end((err, res) => {
        expect(err).to.be.instanceOf(Error)
        expect(res.statusCode).to.be.equal(403)
        var body = res.body
        expect(body).to.be.deep.equal({error: 'not authorized'})
        next()
      })
  })

  it('return err if `till` date in future', (next) => {
    const from = moment().startOf('month').valueOf()
    const till = moment().add(1, 'day').endOf('day').valueOf()
    const expected = {
      error: '`till` date can\'t be in future'
    }

    const url = `${testUrl}?from=${from}&till=${till}`
    request.get(url)
      .set({ Authorization: currUser.token })
      .end((err, res) => {
        expect(err).to.be.instanceOf(Error)
        expect(res.statusCode).to.be.equal(400)
        var body = res.body
        expect(body).to.be.deep.equal(expected)
        next()
      })
  })

  it('return err if `from` date is after `till` date', (next) => {
    const from = moment().subtract(1, 'month').startOf('month').add(1, 'day').valueOf()
    const till = moment().subtract(1, 'month').startOf('month').valueOf()
    const expected = {
      error: '`from` date can\'t be after `till` date'
    }

    const url = `${testUrl}?from=${from}&till=${till}`
    request.get(url)
      .set({ Authorization: currUser.token })
      .end((err, res) => {
        expect(err).to.be.instanceOf(Error)
        expect(res.statusCode).to.be.equal(400)
        var body = res.body
        expect(body).to.be.deep.equal(expected)
        next()
      })
  })
})
