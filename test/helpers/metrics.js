/* global helpers */
'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const requireCfg = $require('lib/requireCfg')
const faker = require('faker')
const bluebird = require('bluebird')
const couchbase = $require('lib/adapters/couchbase')
const metricsCfg = requireCfg('metrics')

const Site = $require('models/site')
const Metric = $require('models/metric')

process.env.LOG_LEVEL = 'info'

function createSiteInCouchbase (siteData) {
  return new Promise((resolve, reject) => {
    couchbase.selectBucket('default').insert(`site_${siteData.shortname}`, siteData, (err) => {
      if (err) { return reject() }

      resolve()
    })
  })
}

function getSite (siteShortName) {
  return new Promise((resolve, reject) => {
    Site.findOne({ shortname: siteShortName }, 'shortname', (err, site) => {
      if (err) { return reject(err) }

      resolve(site)
    })
  })
}

function getImpression (siteShortName) {
  return getSite(siteShortName)
    .then(site => {
      return new Promise((resolve, reject) => {
        Metric.findOne({ site: site._id, metricType: metricsCfg.types['impressions'] }, 'value', (err, impression) => {
            if (err) { return reject(err) }

            resolve(impression)
          }
        )
      })
    })
}

let metricsServer = null
var stopCallTimeoutId = null

function start (next) {
  if (metricsServer) {
    // sever should be started once per folder
    if (stopCallTimeoutId) { clearTimeout(stopCallTimeoutId) }
    return next()
  }

  helpers.cleanDB(function (err) {
    if (err) return next(err)
    if (!metricsServer) {
      $require('metrics')((err, server) => {
        if (err) { console.error(err) }
        metricsServer = server
        process.nextTick(next)
      })
    } else { next() }
  })
}

function stop (next) {
  if (stopCallTimeoutId) { clearTimeout(stopCallTimeoutId) }
  stopCallTimeoutId = setTimeout(stopServer, 1000)
  next()
}

function stopServer () {
  if (metricsServer) {
    metricsServer.close()
    process.exit(0)
  }
}

module.exports = (helpers) => {
  helpers.metrics = {
    createSiteInCouchbase,
    getImpression,
    start: start,
    stop: stop
  }
}