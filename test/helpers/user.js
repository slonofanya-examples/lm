/* global helpers */
'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const faker = require('faker')
const _ = require('lodash')
const request = require('superagent')

const errResNotFound = $require('./lib/api-err-responders/resource-not-found')

let User = $require('models/user')

function activate (userData) {
  return new Promise ((resolve, reject) => {
    User.findByCredentials(userData.email, userData.password)
      .then((user, err) => {
        if (err) { return reject(err) }
        if (!user) {
          const err = errResNotFound(`${userData.email}:${userData.password}`, 'User')
          return reject(err)
        }
        user.active = true
        user.save((err, res) => {
          if (err) { return reject(err) }
          resolve(res)
        })
      })
  })
}

function checkUserDefaults (userData) {
  userData = userData || {}
  return _.chain(userData).clone().defaults({
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    password: faker.internet.password()
  }).value()
}

/**
 * Creates user directly into db
 * userData is object hash of User model properties, where the following will be automatically
 * populated:
 *
 * + firstName
 * + lastName
 * + email
 * + password
 *
 */
function create (userData) {
  return new Promise ((resolve, reject) => {
    var fullUserData = checkUserDefaults(userData)

    User.create(fullUserData, (err, user) => {
      if (err) { return reject(err) }
      resolve(user)
    })
  })
}

function createAndLogin (userData) {
  const fullUserData = checkUserDefaults(userData)
  return create(fullUserData)
    .then(() => {
      return activate(fullUserData) })
    .then(() => {
      return login(fullUserData) })
}

function getToken (user) {
  var jwt = require('jsonwebtoken')
  var userProfile = {
    id: user.id.toString(),
    created: user.created,
    email: user.email
  }
  return jwt.sign(userProfile, 'emmofret.meapiv0')
}

function login (userData) {
  return new Promise ((resolve, reject) => {
    request.post(`${helpers.variables.apiEndpoint}/users/log-in`)
      .send(_.pick(userData, ['email', 'password']))
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          return reject(err)
        }
        if (res.statusCode !== 200) {
          return reject(res.body)
        }

        resolve(res.body)
      })
  })
}

module.exports = (helpers) => {
  helpers.user = {
    getToken: getToken,
    create: create,
    login: login,
    activate: activate,
    createAndLogin: createAndLogin
  }
}
