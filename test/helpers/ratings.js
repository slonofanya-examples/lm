/* global helpers */
'use strict'
const $require = require(`${process.cwd()}/lib/require`)
const faker = require('faker')
const _ = require('lodash')
const moment = require('moment')
const bluebird = require('bluebird')
const mongoose = require('mongoose')

const Site = $require('models/site')
const SiteStat = $require('models/siteStat')

process.env.LOG_LEVEL =  'info'
const log = $require('lib/logger').get('faker')

const sitesAmount = 3

function fakeSiteGen () {
  let id = 0
  return () => {
    const domain = faker.internet.domainName()
    return {
      name : faker.lorem.sentence(),
      shortname : `${domain.replace('.', '-')}-${id++}`,
      url: `http://www.${domain}`
    }
  }
}

function getSiteStatsPromise (site, timestamp) {
  return new Promise((resolve, reject) => {
    const payload = {
      _creator: site._id,
      timestamp: timestamp,
      calls: _.random(0, 1080)
    }

    SiteStat.create(payload, (err, stat) => {
      if (err) { return reject(err) }
      resolve(stat)
    })
  })
}

/**
 * Generates fake stats data for every site for period == last month
 * @param sites - array of models
 * @returns {Promise}
 */
function genSitesStats (sites) {
  const fromDate = moment().subtract(1, 'month').startOf('month')
  const tillDate = moment().subtract(1, 'month').endOf('month')
  const days = tillDate.date()
  const pollingInterval = 60 //min
  const statsAmount = parseInt(days * ( 24*60/pollingInterval ))

  log.info('Prepare promises ..')
  console.time('prepare stats promises')
  let statsPromises = []
  sites.forEach((site) => {
    let date = moment(fromDate)
    statsPromises.push(_.times(statsAmount, (idx) => {
      const timestamp = date.add(pollingInterval, 'minutes').valueOf()
      return getSiteStatsPromise(site, timestamp)
    }))
  })

  statsPromises = _.flatten(statsPromises)
  console.timeEnd('prepare stats promises')
  log.info(`Gonna resolve [${statsPromises.length}] promises ...`)
  console.time('stats promises')
  return Promise.all(statsPromises)
}

function fillDB () {
  function getCreateSitePromise () {
    return new Promise((resolve, reject) => {
      const payload = siteGen()
      Site.create(payload, (err, site) => {
        if (err) { return reject(err) }

        resolve(site)
      })
    })
  }

  let siteGen = fakeSiteGen()
  const sitesPromises = _.times(sitesAmount, getCreateSitePromise)

  log.info('Filling DB with fake sites ...')
  console.time('sites')
  return bluebird.all(sitesPromises)
    .tap((sites) => {
      log.info(`Created [${sites.length}] fake sites`)
      console.timeEnd('sites')
    })
    .then(genSitesStats)
    .tap((siteStats) => {
      log.info(`Created [${siteStats.length}] stats records`)
      console.timeEnd('stats promises')
    })
}

function genFake () {
  return fillDB()
    .then(() => log.info('done!'))
    .catch((err) => log.error(err))
}

module.exports = (helpers) => {
  helpers.ratings = {
    genFake: genFake,
  }
}
