/* global before, after, it, expect, faker, describe, helpers */
'use strict'
var request = require('superagent')

describe('POST metrics/impressions', function () {
  const testUrl = helpers.variables.metricsApiEndpoint + '/metrics/impressions'
  const testData = {
    siteShortName: faker.internet.domainName().replace('.', '-'),
    value: 2
  }
  const couchTestSiteData = {
    "name": testData.siteShortName,
    "url": testData.siteShortName,
    "shortname": testData.siteShortName,
  }
  const expected = {
    "saved": "ok"
  }

  before(helpers.metrics.start)
  before(next => {
    helpers.metrics.createSiteInCouchbase(couchTestSiteData).then(next)
  })
  after(helpers.metrics.stop)

  it('should create impression and increment value', (next) => {
    request.post(testUrl)
      .send(testData)
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.statusCode).to.be.equal(200)
        var body = res.body
        expect(body).to.be.deep.equal(expected)

        helpers.metrics.getImpression(testData.siteShortName)
          .then(data => {
            expect(testData.value).to.be.equal(2)
          })

          // Touch API method one more time and check value
          .then(() => new Promise((resolve, reject) => {
            request.post(testUrl)
              .send(testData)
              .end((err, res) => {
                if (err) { reject(err) }
                resolve(res)
              })
          }))
          .then(() => helpers.metrics.getImpression(testData.siteShortName))
          .then(impression => {
            expect(impression.value).to.be.equal(4)
          })

          .then(next)
          .catch(next)
      })
  })
})