/* global before, after, it, expect, faker, describe, helpers */
'use strict'
var request = require('superagent')

describe('[EDGE CASES] metrics/impressions/create', function () {
  const testUrl = helpers.variables.metricsApiEndpoint + '/metrics/impressions'

  before(helpers.metrics.start)
  after(helpers.metrics.stop)

  it('Fail when post not allowed parameter', (next) => {
    const testData = { some: 'thing' }
    const expected = { "error": "Request params not allowed: [some]" }

    request.post(testUrl)
      .send(testData)
      .end((err, res) => {
        expect(err).not.to.be.null
        expect(res.statusCode).to.be.equal(400)
        var body = res.body
        expect(body).to.be.deep.equal(expected)
        next()
      })
  })

  it('Fail when do not post site short name', (next) => {
    const testData = {}
    const expected = {
      "error": "siteShortName is required"
    }

    request.post(testUrl)
      .send(testData)
      .end((err, res) => {
        expect(err).not.to.be.null
        expect(res.statusCode).to.be.equal(400)
        var body = res.body
        expect(body).to.be.deep.equal(expected)
        next()
      })
  })

  it('Fail when post wrong value', (next) => {
    const testData = {
      siteShortName: faker.internet.domainName().replace('.', '-'),
      value: 'abc'
    }
    const expected = {
      "errors": {
        "value": ["Wrong value format, must be numeric"]
      }
    }

    request.post(testUrl)
      .send(testData)
      .end((err, res) => {
        expect(err).not.to.be.null
        expect(res.statusCode).to.be.equal(422)
        var body = res.body
        expect(body).to.be.deep.equal(expected)
        next()
      })
  })

  it('Site with unknown shortName shouldn\'t be found', (next) => {
    const testData = {
      siteShortName: faker.internet.domainName().replace('.', '-')
    }
    const expected = { "error": `resource [${testData.siteShortName}] not found` }

    request.post(testUrl)
      .send(testData)
      .end((err, res) => {
        expect(err).not.to.be.null
        expect(res.statusCode).to.be.equal(404)
        var body = res.body
        expect(body).to.be.deep.equal(expected)
        next()
      })
  })
})